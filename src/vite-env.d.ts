/// <reference types="vite/client" />
// 为了使使用环境变量时有提示
interface ImportMetaEnv {
  readonly VITE_TEST_VALUE: string
  readonly VITE_API_BASE_URL: string
  readonly VITE_SERVER_PORT: number
  // 更多环境变量...
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}
