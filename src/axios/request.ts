import axios, { AxiosError, AxiosResponse, InternalAxiosRequestConfig } from 'axios'
import { useToast } from 'vue-toastification'
const request = axios.create({
  // 默认请求地址，根据环境的不同可在.env 文件中进行修改
  baseURL: import.meta.env.VITE_API_BASE_URL,
  // 设置接口访问超时时间
  timeout: 4000, // request timeout，
  // 设置请求头中的请求内容类型
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  },
  // 跨域时候允许携带凭证
  withCredentials: true
})

//  request interceptor 接口请求拦截
request.interceptors.request.use(
  (config: InternalAxiosRequestConfig) => {
    return config
  },
  (error: AxiosError) => {
    // TODO Axios 请求错误拦截
    useToast().error(error.message)
    return Promise.reject(error)
  }
)

//  response interceptor 接口响应拦截
request.interceptors.response.use(
  (response: AxiosResponse) => {
    const code: number = response.data.code
    if (code !== 200) {
      useToast().error(response.data.code + ': ' + response.data.message)
      return Promise.reject(response.data)
    }
    return response.data
  },
  (error: AxiosError) => {
    // TODO Axios 响应错误拦截
    useToast().error(error.message)
    console.log(error)
    return Promise.reject(error)
  }
)

export default request
