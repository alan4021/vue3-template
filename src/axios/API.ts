import request from '@/axios/request.ts'

type ResponseType = {
  code: number
  data: any
  message: string
}

export const DetectionJoin = () => {
  return request.request<any, ResponseType>({
    url: '/detection/join',
    method: 'get'
  })
}
