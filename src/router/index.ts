import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

const routes: Readonly<RouteRecordRaw[]> = [
  {
    path: '/',
    name: '',
    redirect: () => {
      return {
        path: '/home'
      }
    }
  },
  {
    path: '/home',
    name: 'Home',
    meta: { requiresAuth: true },
    component: () => import('@v/Home.vue')
  }
]

const Router = createRouter({
  history: createWebHistory(), // history 模式
  routes
})

// 全局前置守卫
Router.beforeEach((to, from) => {
  // 访问认证判断
  if (to.meta.requiresAuth) {
    // TODO 判断是否登录并给出页面提示
    console.log('TODO 访问认证判断', to, from)
  }
  return true
})

// TODO 全局后置守卫
Router.afterEach((to, from) => {
  console.log('TODO RouterAfterEach', to, from)
})

export default Router
