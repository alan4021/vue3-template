import { createApp } from 'vue'
import './style.css'
import App from './App.vue'

const app = createApp(App)
import Router from '@/router'
app.use(Router)

import Pinia from '@/pinia'
app.use(Pinia)

import Toast, { PluginOptions, TYPE } from 'vue-toastification'
// Import the CSS or use your own!
import 'vue-toastification/dist/index.css'
import { ToastOptions } from 'vue-toastification/src/types'
import { POSITION } from 'vue-toastification/src/ts/constants.ts'
// 配置默认参数
const toastDefaultsCommon: ToastOptions = {
  position: POSITION.TOP_RIGHT, // 定义Toast在屏幕上的位置
  draggable: false, // 是否允许用户通过拖动来移动Toast
  pauseOnFocusLoss: true, // 如果设置为true，当窗口失去焦点时，Toast会暂停显示
  pauseOnHover: true, // 若设置为true，鼠标悬停在Toast上时，它将暂停计时
  timeout: 4000, // 自动关闭Toast的延迟时间，以毫秒为单位，或设置为false禁用自动关闭
  hideProgressBar: false, //  是否隐藏进度条
  icon: true
}
const options: PluginOptions = {
  maxToasts: 8,
  transition: 'Vue-Toastification__fade',
  newestOnTop: true,
  toastDefaults: {
    // ToastOptions object for each type of toast
    [TYPE.ERROR]: toastDefaultsCommon,
    [TYPE.SUCCESS]: toastDefaultsCommon,
    [TYPE.WARNING]: toastDefaultsCommon,
    [TYPE.INFO]: toastDefaultsCommon
  }
}
app.use(Toast, options)

app.mount('#app')

console.log(import.meta.env)
