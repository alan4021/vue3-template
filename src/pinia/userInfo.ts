import { defineStore } from 'pinia'
import { parse, stringify } from 'zipson'
const useUserInfoStore = defineStore({
  id: 'userInfo',
  // ref() 就是 state 属性、computed() 就是 getters、function() 就是 actions、return {  }
  state: () => ({
    user: {
      name: 'alan',
      count: 0
    }
  }),
  getters: {
    // name: (state: PiniaCustomStateProperties) => {}
  },
  actions: {
    // name(){}
  },
  persist: {
    key: 'userInfo',
    storage: localStorage,
    paths: ['user'],
    serializer: {
      deserialize: parse,
      serialize: stringify
    },
    //  该hook 将在从 storage 中恢复数据之前触发，并且它可以访问整个 PiniaPluginContext，这可用于在恢复数据之前强制地执行特定的操作
    beforeRestore: (ctx) => {
      console.log('persist beforeRestore', ctx.store.$id)
    },
    // 该 hook 将在从 storage 中恢复数据之后触发，并且它可以访问整个 PiniaPluginContext，这可用于在恢复数据之后强制地执行特定的操作。
    afterRestore: (ctx) => {
      console.log('persist afterRestore ', ctx.store.$id)
    },
    // 当设置为 true 时，持久化/恢复 Store 时可能发生的任何错误都将使用 console.error 输出
    debug: true
  }
})

export default useUserInfoStore
