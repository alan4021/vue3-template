import { createPinia } from 'pinia'
import piniaPersist from 'pinia-plugin-persistedstate' // 持久化插件
const Pinia = createPinia()
Pinia.use(piniaPersist)
export default Pinia // 导出给main.ts使用
